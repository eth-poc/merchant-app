import {useEffect, useState} from 'react'
import {Route, Routes} from "react-router-dom";
import ClientApp from "./components/client-app/ClientApp.jsx";
import MerchantApp from "./components/merchant-app/MerchantApp.jsx";
import MerchantDashboard from "./components/merchant-dashboard/MerchantDashboard.jsx";
import Home from "./components/Home.jsx";
import NavBar from "./components/NavBar.jsx";
import {Web3} from "web3";
import ClientDashboard from "./components/client-dashboard/ClientDashboard.jsx";

const App = () => {
    const [web3, setWeb3] = useState(null);

    useEffect(() => {
        try {
            const web3Instance = new Web3('http://localhost:8545');
            setWeb3(web3Instance);
        } catch (error) {
            alert('Error while connecting to local web3 provider.');
        }
    }, []);
    return (
        <div className="min-h-screen flex flex-col">
            <div className="gradient-bg-welcome">
                <NavBar />
            </div>

            <Routes>
                <Route path="/" exact element={<Home />}></Route>
                <Route path="/home" exact element={<Home />}></Route>
                <Route path="client-dashboard" exact element={<ClientDashboard web3Instance={web3} />}></Route>
                <Route path="/client-app/:qrDetails" element={<ClientApp />}></Route>
                <Route path="/merchant-app" element={<MerchantApp web3Instance={web3} />}></Route>
                <Route path="/merchant-dashboard" element={<MerchantDashboard web3Instance={web3} />}></Route>
            </Routes>
        </div>
    )
}

export default App