import {SHA256} from "crypto-js";

class AppUtils {
    static formatDateTime = (timestamp) => {
        const date = new Date(timestamp);
        const options = {
            year: "numeric",
            month: "long",
            day: "numeric",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
            hour12: false, // Set this to false for 24-hour format
        };
        return date.toLocaleString(undefined, options);
    };

    static generateQRUniqueID() {
        const array = new Uint8Array(32);
        window.crypto.getRandomValues(array);
        return SHA256(Array.from(array).map(b => b.toString(16).padStart(2, '0')).join('')).toString();
    };
}

export default AppUtils;