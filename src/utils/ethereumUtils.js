export const handleAccountsChanged = async (setUserAddressCallback) => {
    const updateAddress = (accounts) => {
        if (accounts.length === 0) {
            console.log('Please connect to MetaMask.');
        } else {
            setUserAddressCallback(accounts[0]);
        }
    };

    if (window.ethereum) {
        window.ethereum
            .request({ method: 'eth_accounts' })
            .then(updateAddress)
            .catch((err) => {
                console.error(err);
            });

        window.ethereum.on('accountsChanged', updateAddress);

        return () => {
            if (window.ethereum) {
                window.ethereum.removeAllListeners('accountsChanged');
            }
        };
    } else if (window.web3) {
        window.web3.eth.getAccounts((err, accounts) => {
            if (err) console.error(err);
            else updateAddress(accounts);
        });
    } else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }
};
