import axios from "axios";

class APIServices {
    static getAllTables() {
        return axios.get('http://localhost:8080/restaurant/restaurant-tables');
    }

    static updateTable(tableDetails) {
        return axios.post('http://localhost:8080/restaurant/restaurant-table-update', tableDetails);
    }

    static generateReceipt(receiptDetails) {
        return axios.post('http://localhost:8080/restaurant/restaurant-generate-bill', receiptDetails);
    }

    static getBill(tableId) {
        return axios.post('http://localhost:8080/restaurant/restaurant-get-bill', tableId)
    }

    static generateBill(bill) {
        return axios.post('http://localhost:8080/bill/generate-bill', bill)
    }
}

export default  APIServices;