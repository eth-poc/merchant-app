export const navLinks = [
    {
        id: "home",
        title: "Home",
    },
    {
        id: "merchant-app",
        title: "Merchant App",
    },
    {
        id: "client-dashboard",
        title: "Client Dashboard",
    },
    {
        id: "merchant-dashboard",
        title: "Merchant Dashboard",
    },
];