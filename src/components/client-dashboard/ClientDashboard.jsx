import React, {useEffect, useState} from 'react';
import BillingPaymentABI from '../../assets/contracts/BillingPayment.json';
import QRCode from "qrcode.react";
import AppUtils from "../../utils/AppUtils.js";
import { handleAccountsChanged } from '../../utils/ethereumUtils';


const contractAddress = '0x1eeb60e97547a40f0a38b120E72a70c719366248';

const initializeContract = (web3Instance) => {
    return new web3Instance.eth.Contract(BillingPaymentABI, contractAddress);
};

const ClientDashboard = ({ web3Instance }) => {
    const [userAddress, setUserAddress] = useState('');
    const [account, setAccount] = useState(null);
    const [payments, setPayments] = useState([]);
    const [selectedBill, setSelectedBill] = useState(null);
    const [selectedPaymentDetails, setSelectedPaymentDetails] = useState(null);

    useEffect(() => {
        handleAccountsChanged(setUserAddress);
    }, []);

    useEffect(() => {
        const fetchPayments = async () => {
            if (userAddress && web3Instance) {
                const contract = initializeContract(web3Instance);
                try {
                    const details = await contract.methods.getReceiptsPerAddress(userAddress).call();
                    console.log(details);
                    setPayments(details);
                } catch (error) {
                    console.error('An error occurred:', error);
                }
            }
        };

        fetchPayments();
    }, [userAddress, web3Instance]);

    const connectWallet = async () => {
        if (window.ethereum) {
            try {
                // Request account access if needed
                await window.ethereum.request({method: 'eth_requestAccounts'});

                // We don't actually assign the accounts variable in this code snippet
                const accounts = await window.ethereum.request({method: 'eth_accounts'});
                setAccount(accounts[0]);
            } catch (error) {
                // User denied account access
                console.error("User denied account access");
            }
        } else {
            console.error("Non-Ethereum browser detected. You should consider trying MetaMask!");
        }
    };

    const handlePaymentClick = async (paymentIndex) => {
        setSelectedPaymentDetails(payments[paymentIndex]);
    };

    return (
        <div className="flex flex-1 flex-col items-left justify-left text-white white-glassmorphism ">
            <div>
                <button className="p-4 m-2 border-2 border-gray-300 square-button" onClick={connectWallet}>Connect MetaMask Wallet</button>
            </div>

            <div className="flex flex-col mt-2">
                {payments.map((payment, index) => (
                    <div key={index} className="p-4 border border-gray-300 rounded mb-2 cursor-pointer">
                        <p>Ref: {payment.uniquePaymentId}</p>
                        <p>Amount: {web3Instance.utils.fromWei(payment.amount.toString(), 'ether')} ETH</p>
                        <p>Date: {AppUtils.formatDateTime(payment.timeStamp * 1)}</p>
                        <p onClick={() => handlePaymentClick(index)}>Bill ref: {payment.billUniqueId}</p>
                    </div>
                ))}
                {selectedPaymentDetails && (
                    <div className="absolute left-30 p-2 bg-white border border-gray-300 rounded shadow-lg">
                        <p className="text-black">Details for payment: {selectedPaymentDetails.uniquePaymentId}</p>
                        <div className="mb-4">
                            <hr className="border-gray-400" />
                            <p className="text-black">Bill ref: {selectedPaymentDetails.billUniqueId}</p>
                            <p className="text-black mt-2">Payed at: {AppUtils.formatDateTime(selectedPaymentDetails.timeStamp * 1)}</p>
                            <p className="text-black">Generated at: {AppUtils.formatDateTime(selectedPaymentDetails.billTimeStamp * 1)}</p>
                            <hr className="border-gray-400" />

                            <div className="bg-gray-100 p-4 rounded-lg my-4">
                                <h3 className="text-black mb-2">Details:</h3>
                                <ul>
                                    {JSON.parse(selectedPaymentDetails.metaData).map((item, index) => (
                                        <li key={index} className="mb-2 flex justify-between">
                                            <span className="text-gray-700">{item.quantity}x {item.name} (@ {item.price.toFixed(2)} ETH)</span>
                                            <span className="text-black">{(item.price * item.quantity).toFixed(2)} ETH</span>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            <p className="text-black">Bill Amount: {web3Instance.utils.fromWei(selectedPaymentDetails.billAmount.toString(), 'ether')} ETH</p>
                            <p className="text-black">Payment amount: {web3Instance.utils.fromWei(selectedPaymentDetails.amount.toString(), 'ether')} ETH</p>
                            <p className="text-black">Tip amount: {web3Instance.utils.fromWei(selectedPaymentDetails.tipAmount.toString(), 'ether')} ETH</p>
                        </div>
                        <button
                            className="absolute top-0 right-0 p-2 bg-red-600 text-white rounded-full hover:bg-red-700 focus:outline-none"
                            onClick={() => setSelectedPaymentDetails(null)}
                        >
                            X
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default ClientDashboard;