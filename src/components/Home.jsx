const Home = () => {
    return (
    <div className="flex w-full justify-center items-center">
        <div className="flex mf:flex-row flex-col items-center justify-center py-6 px-4">
            <div className="flex flex-1 flex-col items-center justify-center">
                <h1 className="text-3xl sm:text-5xl text-center text-white white-glassmorphism">
                    Building the Future, One Block at a Time
                </h1>
            </div>
        </div>
    </div>
    );
}

export default Home;