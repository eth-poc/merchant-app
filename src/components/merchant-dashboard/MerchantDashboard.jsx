import React, {useEffect, useState} from 'react';
import BillingPaymentABI from '../../assets/contracts/BillingPayment.json';
import QRCode from "qrcode.react";
import AppUtils from "../../utils/AppUtils.js";
import { handleAccountsChanged } from '../../utils/ethereumUtils';


const contractAddress = '0x1eeb60e97547a40f0a38b120E72a70c719366248';

const initializeContract = (web3Instance) => {
    return new web3Instance.eth.Contract(BillingPaymentABI, contractAddress);
};

const MerchantDashboard = ({ web3Instance }) => {
    const [userAddress, setUserAddress] = useState('');
    const [account, setAccount] = useState(null);
    const [bills, setBills] = useState([]);
    const [selectedBill, setSelectedBill] = useState(null);
    const [selectedPaymentDetails, setSelectedPaymentDetails] = useState(null);

    useEffect(() => {
        handleAccountsChanged(setUserAddress);
    }, []);

    useEffect(() => {
        const fetchBills = async () => {
            if (userAddress && web3Instance) {
                const contract = initializeContract(web3Instance);
                try {
                    const details = await contract.methods.getBillsForMerchant(userAddress).call();
                    setBills(details);
                } catch (error) {
                    console.error('An error occurred:', error);
                }
            }
        };

        fetchBills();
    }, [userAddress, web3Instance]);

    const renderBill = (bill, index) => {
        let statusColor;
        let category;
        if (bill.processed) {
            statusColor = 'bg-green-500';
            category = 'Paid';
        } else if (bill.payments && bill.payments.length > 0) {
            statusColor = 'bg-yellow-500';
            category = 'Pending';
        } else {
            statusColor = 'bg-red-500';
            category = 'Unpaid';
        }

        return (
            <div key={index} className={`p-4 m-2 rounded ${statusColor}`} onClick={() => setSelectedBill(bill)}>
                <h1>{bill.billUniqueId.substring(0, 8)}</h1>
            </div>
        );
    };

    const billsCategories = {
        Paid: bills.filter(bill => bill.processed),
        Pending: bills.filter(bill => !bill.processed && bill.payments && bill.payments.length > 0),
        Unpaid: bills.filter(bill => !bill.processed && (!bill.payments || bill.payments.length === 0))
    };

    const connectWallet = async () => {
        if (window.ethereum) {
            try {
                // Request account access if needed
                await window.ethereum.request({method: 'eth_requestAccounts'});

                // We don't actually assign the accounts variable in this code snippet
                const accounts = await window.ethereum.request({method: 'eth_accounts'});
                setAccount(accounts[0]);
            } catch (error) {
                // User denied account access
                console.error("User denied account access");
            }
        } else {
            console.error("Non-Ethereum browser detected. You should consider trying MetaMask!");
        }
    };

    const handlePaymentClick = async (paymentId, paymentIndex) => {
        if (userAddress && web3Instance) {
            const contract = initializeContract(web3Instance);
            try {
                const details = await contract.methods.getPaymentDetails(paymentId).call();
                console.log(details);
                setSelectedPaymentDetails({ id: paymentId, index: paymentIndex, details: details });
            } catch (error) {
                console.error('An error occurred:', error);
            }
        }
    };


    return (
        <div className="flex flex-1 flex-col items-left justify-left text-white white-glassmorphism ">
                <div>
                    <button className="p-4 m-2 border-2 border-gray-300 square-button" onClick={connectWallet}>Connect MetaMask Wallet</button>
                </div>
                <div className="flex flex-col mt-2">
                    {Object.entries(billsCategories).map(([category, bills]) => (
                        <div key={category}>
                            <h2 className={`text-${category === 'Paid' ? 'green' : category === 'Pending' ? 'yellow' : 'red'}-500`}>{category} Bills</h2>
                            <div className="flex flex-wrap justify-left">
                                {bills.map((bill, index) => renderBill(bill, index))}
                            </div>
                        </div>
                    ))}

                    {selectedBill && (
                                <div className="fixed inset-0 bg-gray-900 bg-opacity-50 flex justify-start items-center">
                                    <div className="bg-white p-4 rounded-lg shadow-lg max-w-5xl w-full relative">
                                        <h2 className="text-black">Bill Details(Ref:{selectedBill.billUniqueId})</h2>
                                        <p className="text-black">Payed amount: {web3Instance.utils.fromWei(selectedBill.payedAmount.toString(), 'ether')} ETH</p>
                                        <p className="text-black">Date and time: {AppUtils.formatDateTime(selectedBill.billTimeStamp * 1)}</p>
                                        <div className="text-black">Payments:
                                            {selectedBill.payments.map((paymentId, paymentIndex) => (
                                                <li key={paymentIndex} className="mb-2 flex justify-between">
                                                    <span className="text-gray-700 relative cursor-pointer" onClick={() => handlePaymentClick(paymentId, paymentIndex)}>
                                                        {paymentId.substring(0, 8)}
                                                    </span>

                                                </li>
                                            ))}
                                            {selectedPaymentDetails && (
                                                <div className="absolute left-30 p-2 bg-white border border-gray-300 rounded shadow-lg">
                                                    <p>Details for payment: {selectedPaymentDetails.id}</p>
                                                    <div className="mb-4">
                                                        <hr className="border-gray-400" />
                                                        <p className="text-black mt-2">Date and time: {AppUtils.formatDateTime(selectedPaymentDetails.details.timeStamp * 1)}</p>
                                                        <p className="text-black mt-2">Wallet: {selectedPaymentDetails.details.clientAddress}</p>
                                                        <p className="text-black mt-2">Total: {web3Instance.utils.fromWei(selectedPaymentDetails.details.amount.toString(), 'ether')} ETH</p>
                                                    </div>
                                                    <button
                                                        className="absolute top-0 right-0 p-2 bg-red-600 text-white rounded-full hover:bg-red-700 focus:outline-none"
                                                        onClick={() => setSelectedPaymentDetails(null)}
                                                    >
                                                        X
                                                    </button>
                                                </div>
                                            )}
                                        </div>
                                        <div className="bg-gray-100 p-4 rounded-lg my-4">
                                            <h3 className="text-black mb-2">Order Details:</h3>
                                            <ul>
                                                {JSON.parse(selectedBill.metaData).map((item, index) => (
                                                    <li key={index} className="mb-2 flex justify-between">
                                                        <span className="text-gray-700">{item.quantity}x {item.name} (@ {item.price.toFixed(2)} ETH)</span>
                                                        <span className="text-black">{(item.price * item.quantity).toFixed(2)} ETH</span>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>

                                        <div className="mb-4">
                                            <hr className="border-gray-400" />
                                            <p className="text-black mt-2">Total: {web3Instance.utils.fromWei(selectedBill.billAmount.toString(), 'ether')} ETH</p>
                                            <p className="text-black">Tip: {web3Instance.utils.fromWei(selectedBill.tipAmount.toString(), 'ether')} ETH</p>
                                            <p className="text-black">Tax: {web3Instance.utils.fromWei(selectedBill.txAmount.toString(), 'ether')} ETH</p>
                                            <p className="text-black">Revenue: {web3Instance.utils.fromWei(selectedBill.revenueAmount.toString(), 'ether')} ETH</p>
                                        </div>
                                        <button
                                            className="absolute top-2 right-2 p-2 bg-red-600 text-white rounded-full hover:bg-red-700 focus:outline-none"
                                            onClick={() => setSelectedBill(null)}
                                        >
                                            X
                                        </button>
                                    </div>
                                </div>
                            )}
                </div>
        </div>
    );
}

export default MerchantDashboard;