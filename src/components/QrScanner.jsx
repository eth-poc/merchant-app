import React, {useEffect, useRef, useState} from 'react';
import { BrowserMultiFormatReader } from '@zxing/library';


function QrScanner({onScan}) {
    const [result, setResult] = useState(null);
    const codeReader = new BrowserMultiFormatReader();

    useEffect(() => {
        codeReader.getVideoInputDevices()
            .then(videoInputDevices => {
                if (videoInputDevices.length >= 1) {
                    codeReader.decodeFromVideoDevice(videoInputDevices[0].deviceId, 'video', (result, err) => {
                        if (result) {
                            setResult(result.text);
                        }
                    });
                }
            });

        return () => {
            // Cleanup
            codeReader.reset();
        };
    }, []);

    return (
        <div>
            <video id="video" width="300" height="200"></video>
            <p>{result}</p>
        </div>
    );
}

export default QrScanner;