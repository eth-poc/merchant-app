import React, {Component, useEffect, useState} from 'react';
import AppUtils from "../../utils/AppUtils.js";
import APIServices from "../../services/APIServices.js";
import pako from 'pako';
import QRCode from 'qrcode.react';
import {SHA256} from "crypto-js";

const MerchantApp = () => {
    const [state, setState] = useState({
        showReceipt: false,
        itemsOrdered: [],
        tipPercentage: 15,
        subtotal: 0,
        tip: 0,
        total: 0,
        receiptInput: '',
        qrValues: [],
        billUniqueId: '',
        splitState: 'N',
        inputValues: [],
        remainingAmount: 0,
    });

    const menuItems = {
        PIZZA: 1,
        COLA: 0.5,
        PASTA: 0.23,
        WATER: 0.10,
    };

    const { showReceipt, itemsOrdered, tipPercentage, subtotal, tip, total, qrValues,
        splitState, billUniqueId, inputValues, remainingAmount } = state;

    const addItemToOrder = (item) => {
        const updatedOrder = [...itemsOrdered];
        const existingItemIndex = updatedOrder.findIndex(orderItem => orderItem.name === item.name);
        if (existingItemIndex > -1) {
            updatedOrder[existingItemIndex].quantity += 1;
        } else {
            updatedOrder.push({ ...item, quantity: 1 });
        }

        const newSubtotal = updatedOrder.reduce((acc, item) => acc + (menuItems[item.name] * item.quantity), 0);
        const newTip = (newSubtotal * (tipPercentage / 100)).toFixed(2);
        const newTotal = (newSubtotal + parseFloat(newTip)).toFixed(2);

        setState({ ...state, itemsOrdered: updatedOrder, subtotal: newSubtotal.toFixed(2), tip: newTip, total: newTotal });
    }

    const toggleReceipt = () => {
        setState((prevState) => ({...prevState, showReceipt: !prevState.showReceipt}));
    }

    const setSplit = (splitType) => {


        const createQrDetails = (billId, amount, splitType, splitNumber) => {
            const host = '127.0.0.1'; // replace with your dynamic host mechanism
            const details = {
                billUniqueId: billId,
                amount: (amount*1).toFixed(4),
                uniquePaymentId: AppUtils.generateQRUniqueID(),
                splitType: splitType,
                splitNumber: splitNumber,
            };

            const serializedDetails = JSON.stringify(details);
            //const encodedDetails = compressAndEncode(serializedDetails);

            const encodedDetails = encodeURIComponent(btoa(serializedDetails));

            return `/#client-app/${encodedDetails}`;
        };

        const generateQrValues = (billId, totalAmount, splitType) => {


            if (splitType === 'N') {
                return [(createQrDetails(billId, totalAmount, "H", 0))];
            } else if (splitType === 'H') {
                return [
                    (createQrDetails(billId, totalAmount / 2, "H", 0)),
                    (createQrDetails(billId, totalAmount / 2, "H", 1))
                ];
            }
        };

        const handleBillGeneration = (billId) => {
            const newQrValues = generateQrValues(billId, total, splitType);
            setState((prevState) => ({
                ...prevState,
                qrValues: newQrValues,
                splitState: splitType
            }));
        };

        if (billUniqueId && splitType !== splitState) {
            // If there is already a billUniqueId and splitType has changed, just generate new QR values
            handleBillGeneration(billUniqueId);
        } else {
            // If there is no billUniqueId or splitType hasn't changed, generate a new bill
            const receipt = {
                total: total,
                tip: tip,
                mid: "123456789",
                sub_total: subtotal,
                meta_data: itemsOrdered,
                split_type: splitType,
                time_stamp: Date.now(),
            };

            APIServices.generateBill(receipt).then((response) => {
                const newBillUniqueId = response.data.bill_unique_reference;
                handleBillGeneration(newBillUniqueId);
                // Update billUniqueId in the state if it's a new one
                if (!billUniqueId) {
                    setState((prevState) => ({...prevState, billUniqueId: newBillUniqueId}));
                }
            });
        }
    };

    const handleTipPercentageChange = (e) => {
        const newTipPercentage = e.target.value;
        const newTip = (subtotal * (parseFloat(newTipPercentage) / 100)).toFixed(2);
        const newTotal = (parseFloat(subtotal) + parseFloat(newTip)).toFixed(2);

        setState({ ...state, tipPercentage: newTipPercentage, tip: newTip, total: newTotal });
    }

    return (
        <div className="flex flex-1 flex-col items-center justify-center text-white white-glassmorphism mt-6">
            <div className="flex mt-2">
                {Object.keys(menuItems).map((item, index) => (
                    <button key={index} className="p-4 m-2 border-2 border-gray-300 square-button" onClick={() => addItemToOrder({ name: item, price: menuItems[item] })}>
                        {item}
                    </button>
                ))}
            </div>

            <button onClick={toggleReceipt} className="mt-4">Receipt</button>

            {showReceipt && (
                <div className="flex flex-1 flex-col items-center justify-center text-white white-glassmorphism mt-4">
                    <input
                        type="number"
                        value={tipPercentage}
                        onChange={handleTipPercentageChange}
                        placeholder="Tip Percentage"
                        className="mt-2 p-2 text-black"
                    />
                    <h1 className="text-3xl sm:text-5xl text-center">**********<br/>RECEIPT<br/>**********<br/></h1>
                    <div>
                        <h2>Items Ordered:</h2>
                        <ul>
                            {itemsOrdered.map((item, index) => (
                                <li key={index}>{item.name} -  ETH {menuItems[item.name]} x {item.quantity}</li>
                            ))}
                        </ul>
                    </div>

                    <p>Subtotal: ETH {subtotal}</p>
                    <p>Tip: ETH {tip}</p>
                    <p>Total: ETH {total}</p>

                    <button className="p-4 m-2 border-2 border-gray-300 square-button" onClick={() => setSplit('N')}>
                        Dont split
                    </button>
                    <button className="p-4 m-2 border-2 border-gray-300 square-button" onClick={() => setSplit('H')}>
                        Split by half
                    </button>
                    {qrValues && qrValues.length > 0 && qrValues.map((qrValue, index) => (
                        <React.Fragment key={index}>
                            <QRCode value={qrValue} size={256} />
                            <div>
                                <a href={qrValue} target="_blank" rel="noopener noreferrer">Pay QR</a>
                            </div>
                            {index < qrValues.length - 1 && (
                                <>
                                    <div>QR {index + 1}</div>
                                    <hr />
                                </>
                            )}
                        </React.Fragment>
                    ))}
                </div>
            )}
        </div>
    );
}

export default MerchantApp;
