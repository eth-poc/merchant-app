import React from "react";
import {navLinks} from "./index.js";
import logoMedium from "../assets/logo-medium.png";
import {AiOutlineClose} from "react-icons/ai";
import {HiMenuAlt4} from "react-icons/hi";

const NavBar = () => {
    const [toggleMenu, setToggleMenu] = React.useState(false);
    const [active, setActive] = React.useState("Home");

    return (
        <nav className="w-full flex justify-between items-center p-4">
            <div className="flex items-center">
                <img
                    src={logoMedium}
                    width="140"
                    height="138"
                    className=""
                />
            </div>
            <ul className="text-white md:flex hidden list-none flex-row justify-between items-center flex-initial">
                {navLinks.map((item, index) => (
                    <li
                        key={item.id}
                        className={`hover:text-orange-500 cursor-pointer text-[16px] ${
                            active === item.title ? "text-white" : "text-dimWhite"
                        } ${index === navLinks.length - 1 ? "mr-0" : "mr-10"}`}
                        onClick={() => setActive(item.title)}
                    >
                        <a href={`#${item.id}`}>{item.title}</a>
                    </li>
                ))}
            </ul>
            <div className="flex relative">
                {!toggleMenu && (
                    <HiMenuAlt4 fontSize={28} className="text-white md:hidden cursor-pointer" onClick={() => setToggleMenu(true)} />
                )}
                {toggleMenu && (
                    <AiOutlineClose fontSize={28} className="text-white md:hidden cursor-pointer" onClick={() => setToggleMenu(false)} />
                )}
                {toggleMenu && (
                    <ul
                        className="z-10 fixed -top-0 -left-2 p-3 w-[70vw] h-screen shadow-2xl md:hidden list-none
            flex flex-col justify-start items-end rounded-md orange-glassmorphism text-white animate-slide-in"
                    >
                        {navLinks.map((item, index) => (
                            <li
                                key={item.id}
                                className={`hover:text-orange-500 cursor-pointer text-[16px] ${
                                    active === item.title ? "text-white" : "text-black"
                                } ${index === navLinks.length - 1 ? "mr-0" : "mr-0"}`}
                                onClick={() => setActive(item.title)}
                            >
                                <a href={`#${item.id}`}>{item.title}</a>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        </nav>
    )
}

export default NavBar