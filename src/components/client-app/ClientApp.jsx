import {useParams} from "react-router-dom";
import {Web3} from "web3";
import React, {useEffect, useState} from 'react';
import BillingPaymentABI from "../../assets/contracts/BillingPayment.json";
import AppUtils from "../../utils/AppUtils.js";


const contractAddress = '0x1eeb60e97547a40f0a38b120E72a70c719366248';
const web3Instance = new Web3('http://localhost:8545');

const getContractInstance = () => {
    return new web3Instance.eth.Contract(BillingPaymentABI, contractAddress);
}

const ClientApp = () => {
    const { qrDetails } = useParams();
    const [userAddress, setUserAddress] = useState('');
    const [account, setAccount] = useState('');
    const [payment, setPayment] = useState(null);
    const [billDetails, setBillDetails] = useState(null);

    useEffect(() => {
        try {
            const paymentDetails = JSON.parse(atob(decodeURIComponent(qrDetails)));
            setPayment(paymentDetails);

            if(paymentDetails) {
                const contract = getContractInstance();
                contract.methods.getBillDetails(paymentDetails.billUniqueId)
                    .call()
                    .then(setBillDetails)
                    .catch(console.error);
            }
        } catch (error) {
            console.log(error);
        }
    }, [qrDetails]);

    useEffect(() => {
        const handleAccountsChanged = (accounts) => {
            if (accounts.length === 0) {
                console.log('Please connect to MetaMask.');
            } else {
                setUserAddress(accounts[0]);
            }
        };

        const getAccounts = async () => {
            if (window.ethereum) {
                const accounts = await window.ethereum.request({ method: 'eth_accounts' });
                handleAccountsChanged(accounts);
                window.ethereum.on('accountsChanged', handleAccountsChanged);
            } else if (window.web3) {
                window.web3.eth.getAccounts((err, accounts) => {
                    if (err) console.error(err);
                    else handleAccountsChanged(accounts);
                });
            } else {
                console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
            }
        };

        getAccounts();

        return () => {
            if (window.ethereum) {
                window.ethereum.removeAllListeners('accountsChanged');
            }
        };
    }, []);

    const connectWallet = async () => {
        if (window.ethereum) {
            try {
                await window.ethereum.request({method: 'eth_requestAccounts'});
                const accounts = await window.ethereum.request({method: 'eth_accounts'});
                setAccount(accounts[0]);
            } catch (error) {
                console.error("User denied account access");
            }
        } else {
            console.error("Non-Ethereum browser detected. You should consider trying MetaMask!");
        }
    };

    const pay = async () => {
        if (payment) {
            const contract = getContractInstance();
            const weiAmount = web3Instance.utils.toWei(payment.amount.toString(), 'ether');

            const estimatedGas = await contract.methods.pay(
                payment.billUniqueId,
                payment.uniquePaymentId,
                weiAmount,
                payment.splitNumber,
                payment.splitType,
                Date.now().toString()
            ).estimateGas({
                from: userAddress,
                value: weiAmount
            });

            const transactionParameters = {
                to: contractAddress,
                from: userAddress,
                value: weiAmount,
                gas: estimatedGas,
            };

            contract.methods.pay(
                payment.billUniqueId,
                payment.uniquePaymentId,
                weiAmount,
                payment.splitNumber,
                payment.splitType,
                Date.now().toString()
            ).send(transactionParameters)
                .on('receipt', (receipt) => console.log('Receipt:', receipt))
                .on('error', console.error);
        }
    }

            return (
        <div className="flex flex-1 flex-col items-left justify-left text-white white-glassmorphism mt-6">
            <div>
                <button className="p-4 m-2 border-2 border-gray-300 square-button" onClick={connectWallet}>
                    Connect MetaMask Wallet
                </button>
                {account && <p>Connected: {account}</p>}
            </div>
            {payment && billDetails && (
                <div className="fixed inset-0 bg-gray-900 bg-opacity-50 flex  justify-center">
                    <div className="bg-white p-2 rounded shadow-lg max-w-[60rem] w-full relative">
                        <h2 className="text-black">Payment Details(Ref:{payment.uniquePaymentId})</h2>
                        <p className="text-black">Bill Details(Ref:{payment.billUniqueId})</p>
                        <p className="text-black">Date and time: {AppUtils.formatDateTime(billDetails.billTimeStamp * 1)}</p>
                        <div className="bg-gray-100 p-4 rounded mb-4">
                            <h3 className="text-black mb-2">Order Details:</h3>
                            <ul>
                                {JSON.parse(billDetails.metaData).map((item, index) => (
                                    <li key={index} className="mb-2 flex justify-between">
                                        <span className="text-gray-700">{item.quantity}x {item.name} (@ {item.price.toFixed(2)} ETH)</span>
                                        <span className="text-black">{(item.price * item.quantity).toFixed(2)} ETH</span>
                                    </li>
                                ))}
                            </ul>
                        </div>

                        <div className="mb-4">
                            <hr />
                            <p className="text-black mt-2">Bill Total: {web3Instance.utils.fromWei(billDetails.billAmount.toString(), 'ether')} ETH</p>
                            <p className="text-black">Tip: {web3Instance.utils.fromWei(billDetails.tipAmount.toString(), 'ether')} ETH</p>
                            <p className="text-black">Tax: {web3Instance.utils.fromWei(billDetails.txAmount.toString(), 'ether')} ETH</p>
                        </div>

                        <div className="text-black mt-2 flex justify-center">
                            <button
                                className="p-4 m-2 border-2 border-gray-300 bg-green-500 text-white rounded" onClick={() => pay()}
                            >
                                Pay amount ({payment.amount}) ETH
                            </button>
                        </div>

                    </div>
                </div>
            )}
        </div>
    );
};

export default ClientApp;